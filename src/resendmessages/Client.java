package resendmessages;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

/**
 * Клиентская асть приложения
 * @author Иванчин Егор 17ИТ18
 */
public class Client {
    public static void main(String[] args) {
        String str = null;
        try (Socket socket = new Socket("127.0.0.1", 8080)) {
            if (socket.isConnected()) {
                System.out.println("Подключение к серверу прошло успешно");
            }
            try (Scanner sc = new Scanner(System.in);) {
                System.out.println("введите сообщение для пользователя");
                str = sc.nextLine();
            } catch (Exception e) {
                System.out.println("Ошибка ввода");
            }
            try (InputStream inputStream = socket.getInputStream();
                 OutputStream outputStream = socket.getOutputStream()) {

                outputStream.write(str.getBytes());

                outputStream.flush();

                byte[] data = new byte[32 * 1024];
                int readBytes = inputStream.read(data);

                System.out.printf("increament.Server : " + new String(data, 0, readBytes));
            }
        } catch (IOException e) {
            System.out.println("проблема с сервером");
        }

    }
}