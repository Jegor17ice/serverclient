package resendmessages;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Серверная часть приложения
 */
public class Server {
    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket(8080);
        System.out.println("Сервер ждет клиента...");
        try (Socket clientSocket = serverSocket.accept()) {
            System.out.println("Новое соединение: " + clientSocket.getInetAddress().toString());


            try (InputStream inputStream = clientSocket.getInputStream();
                 OutputStream outputStream = clientSocket.getOutputStream()) {
                byte[] buf = new byte[32 * 1024];
                int readBytes = inputStream.read(buf);
                String line = new String(buf, 0, readBytes);
                System.out.printf("increament.Client : " + line);
                outputStream.write(line.getBytes());
                outputStream.flush();
                System.out.println("");
                System.out.println("клиент отключился");
            }
        }
    }
}